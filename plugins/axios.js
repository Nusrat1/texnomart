export default function ({ app }, inject) {
  // eslint-disable-next-line no-unused-vars
  const { $auth, $axios, $cookies } = app
  // const authStrategy = $cookies.get('auth.strategy')

  $axios.onRequest((config) => {
    // console.log('Base URL', config.baseURL, config.method, config.url)

    $axios.setHeader('Content-Type', 'application/json')

    const authToken = $cookies?.get(`auth._token.local`)
    // const strategyToken = $cookies?.get(`auth._token.${authStrategy}`)
    // console.log('authToken', authToken)
    // console.log(strategyToken)
    $axios.setHeader('Authorization', authToken)
  })

  $axios.onResponseError((error) => {
    console.log('onResponseError', error?.response?.data)
    console.log('onResponseError', error)
    try {
      if (error?.response?.status === 401) {
        if ($auth?.loggedIn && $auth.refreshToken.get()) {
          return $auth.refreshTokens()
        } else {
          // window.location.reload()
        }
        $auth.logout()
        $auth.strategy.token.reset()
        $cookies.removeAll()
      }
    } catch (err) {
      console.error('[Axios] onResponseError', err)
    }
  })
}
