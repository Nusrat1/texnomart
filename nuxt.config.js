export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 3000, // default: 3005
  },
  ssr: false,
  head: {
    title: 'Test',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
  },
  loading: {
    color: 'blue',
    height: '4px',
  },
  mode: 'universal',
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/style/style.scss'],
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/axios.js', { src: '~/plugins/vuelidate.js', ssr: true }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules

  buildModules: [
    // https://go.nuxtjs.dev/eslint
    [
      '@nuxtjs/eslint-module',
      {
        threads: true,
      },
    ],
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'cookie-universal-nuxt',
    '@nuxtjs/dayjs',
    '@nuxtjs/auth-next',
  ],
  auth: {
    // plugins: ["~/plugins/auth/index.js"],
    redirect: {
      login: '/callback',
      logout: '/auth',
      home: '/',
    },
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'token',
          global: false,
        },
        user: {
          property: false,
          autoFetch: true,
        },
        refreshToken: {
          property: false,
        },
        endpoints: {
          login: { url: '/auth/login', method: 'post' },
          user: false,
          logout: false,
        },
      },
    },
    localStorage: false,
  },
  dayjs: {
    locales: ['en', 'ru'],
    defaultLocale: 'en',
    defaultTimeZone: 'Asia/Tashkent',
    plugins: [
      'utc', // import 'dayjs/plugin/utc'
      'timezone', // import 'dayjs/plugin/timezone'
    ], // Your Day.js plugin
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL:
      process.env.VUE_APP_BASE || 'https://jsonplaceholder.typicode.com/',
  },
  env: {
    VUE_APP_BASE: process.env.VUE_APP_BASE,
  },
  publicRuntimeConfig: {
    axios: {
      retry: { retries: 0 },
      baseURL: process.env.VUE_APP_BASE,
    },
  },

  privateRuntimeConfig: {
    axios: {
      retry: { retries: 0 },
      baseURL: process.env.VUE_APP_BASE,
    },
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    extractCSS: {
      ignoreOrder: true,
    },
  },
}
