const state = {
  tasks: [],
}

const getters = {
  fetchedTasks(state) {
    return state.tasks.reverse()
  },
}
const actions = {
  getTasks(store, params) {
    return this.$axios.$get(`/todos/user/1`, { params }).then((response) => {
      store.commit('SET_TASKS', response.todos)
    })
  },
  addTask(store, payload) {
    return this.$axios.$post(`/todos/add`, payload).then(() => {
      const todolist = this.$cookies?.get('tasks')
      todolist.push(payload)
      this.$cookies?.remove('tasks')
      store.commit('SET_TASKS', todolist.reverse())
    })
  },
  updateTask(store, payload) {
    // return this.$axios.$patch(`/todos/${payload.id}/`, payload)
    const todolist = this.$cookies?.get('tasks')
    const index = todolist.findIndex((item) => item.id === payload.id)
    todolist[index] = {
      ...payload,
      completed: !payload.completed,
    }
    this.$cookies?.remove('tasks')
    store.commit('SET_TASKS', todolist)
  },
  deleteTask(store, id) {
    const todolist = this.$cookies?.get('tasks')
    const index = todolist.findIndex((item) => item.id === id)
    console.log(index)
    todolist.splice(index, 1)
    this.$cookies?.remove('tasks')
    store.commit('SET_TASKS', todolist)
  },
}
const mutations = {
  SET_TASKS(state, paylaod) {
    if (!this.$cookies?.get('tasks')) {
      this.$cookies?.set('tasks', paylaod)
      state.tasks = paylaod
    } else {
      state.tasks = this.$cookies?.get('tasks')
    }
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
