const state = {
  products: [],
  detail: null,
  comments: [],
}

const getters = {}
const actions = {
  getProducts(store, params = {}) {
    return this.$axios.$get(`/products`, { params }).then((response) => {
      if (params.brand) {
        const res = response.products.filter((x) => x.brand === params.brand)
        store.commit('SET_PRODUCTS', {
          ...response,
          products: res,
          total: res.length,
        })
      } else {
        store.commit('SET_PRODUCTS', response)
      }
    })
  },
  getDetail(store, id) {
    return this.$axios.$get(`/products/${id}`).then((res) => {
      store.commit('SET_DETAIL', res)
    })
  },
}
const mutations = {
  SET_PRODUCTS(state, paylaod) {
    state.products = paylaod
  },
  SET_DETAIL(state, paylaod) {
    state.detail = paylaod
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
