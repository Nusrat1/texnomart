const state = {
  messages: {
    visible: false,
    text: 'Ok',
    type: 'success',
    duration: 3000,
  },
  cart: JSON.parse(localStorage.getItem('cart')) || null,
}
const getters = {}
const actions = {
  addToCart(store, payload) {
    if (localStorage.getItem('cart')) {
      if (
        JSON.parse(localStorage.getItem('cart')).user ===
        this.$cookies.get('user').id
      ) {
        const cart = JSON.parse(localStorage.getItem('cart'))
        const a = payload
        if (!a.quantity) a.quantity = 1

        const isTrue = cart.products.some(function (e) {
          return e.id === payload.id
        })

        if (isTrue) {
          cart.products.forEach((element) => {
            if (element.id === payload.id) {
              element.quantity++
            }
          })
        } else {
          cart.products.push(a)
        }
        store.commit('SET_CART', cart)
      } else {
        const cart = {
          user: this.$cookies.get('user').id,
          products: [],
        }
        const a = payload
        a.quantity = 1
        cart.products.push(a)
        store.commit('SET_CART', cart)
      }
    } else {
      const cart = []

      const a = payload
      a.quantity = 1
      cart.push(a)
      const newObj = {}
      newObj.user = this.$cookies.get('user').id
      newObj.products = cart
      localStorage.setItem('cart', JSON.stringify(newObj))
    }
  },
  minusFromCart(store, payload) {
    const cart = JSON.parse(localStorage.getItem('cart'))
    cart.products.forEach((element) => {
      if (payload.id === element.id) {
        element.quantity--
      }
    })

    cart.products.forEach((element, index) => {
      if (element.quantity <= 0) {
        cart.products.splice(index, 1)
      }
    })
    store.commit('SET_CART', cart)
    if (!JSON.parse(localStorage.getItem('cart')).products.length) {
      store.commit('SET_CART', undefined)
    }
  },
}
const mutations = {
  Toast(state, messages) {
    state.messages = messages
  },
  SET_CART(state, data) {
    if (data === undefined) {
      state.cart = []
      localStorage.removeItem('cart')
    } else {
      localStorage.removeItem('cart')
      localStorage.setItem('cart', JSON.stringify(data))
      state.cart = data
    }
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}
